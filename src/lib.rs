use std::fs;
use std::io::prelude::*;

pub fn run(config: Config) -> std::io::Result<()> {
    // Load the latex template
    const LATEX_TEMPLATE: &str = include_str!("template.tex");

    let template = LATEX_TEMPLATE.clone();
    let template = template.replace("#\\AUTHOR#", &config.author);
    let template = template.replace("#\\TITLE#", &config.project_name);
    let template = template.replace("#\\DATE#", &config.date);

    // Create the project directory
    fs::create_dir("doc")?;

    // Create the chapters directory
    fs::create_dir("doc/chapters")?;

    // Create the first chapter
    fs::File::create("doc/chapters/einleitung.tex")?;

    // Create the base .tex file
    let file_name = "main.tex";
    let mut file = fs::File::create("doc/".to_owned() + &file_name)?;
    file.write_all(template.as_bytes())?;

    Ok(())
}

pub struct Config {
    pub project_name: String,
    pub author: String,
    pub date: String
}