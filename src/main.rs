use std::{process};
use clap::{Arg, App};
use ratex::Config;

fn main() {
    let matches = App::new("Ratex")
                    .version("1.0")
                    .author("Luca Beetz")
                    .about("Creates a new LaTeX project")
                    .arg(Arg::with_name("title")
                        .help("Sets the title of the project")
                        .required(true))
                    .arg(Arg::with_name("author")
                        .help("Sets the author of the project")
                        .required(true))
                    .arg(Arg::with_name("date")
                        .help("Sets the date of the project")
                        .required(true))
                    .get_matches();

    let config = Config{
        project_name: matches.value_of("title").unwrap().to_string(),
        author: matches.value_of("author").unwrap().to_string(),
        date: matches.value_of("date").unwrap().to_string()
    };

    ratex::run(config).unwrap_or_else(|err| {
        println!("An error occured: {}", err);
        process::exit(1);
    });
}
