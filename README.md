# Ratex

This is a simple command-line tool for creating new LaTeX projects

## Usage

```
ratex <TITLE> <AUTHOR> <DATE>
```